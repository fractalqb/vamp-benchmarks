package main

import (
	"encoding/json"
	"flag"
	"math"
	"math/rand"
	"os"
	"strings"
	"time"

	data "codeberg.org/fractalqb/vamp-benchmarks/partialread"
)

func randString(min, max int) string {
	len := min + rand.Intn(max-min)
	var sb strings.Builder
	for i := 0; i < len; i++ {
		c := 'A' + rand.Intn(26)
		sb.WriteByte(byte(c))
	}
	return sb.String()
}

func rngOrder() (o data.Order) {
	o.CustNum = rand.Uint32()
	o.CustName = randString(8, 40)
	o.Date = time.Now().Add(time.Duration(rand.Intn(8000)-4000) * time.Minute)
	o.Date = o.Date.Round(time.Second)
	lno := 1 + rand.Intn(30)
	for lno > 0 {
		ol := data.OrderLine{
			MatName: randString(5, 20),
			Count:   1 + rand.Intn(1000),
			Price:   0.1 + 5000*rand.Float64(),
		}
		if rand.Float64() < mat0Rate {
			ol.MatNum = 0
		} else {
			for {
				ol.MatNum = rand.Uint32()
				if ol.MatNum != 0 {
					break
				}
			}
		}
		ol.Price = math.Round(100*ol.Price) / 100
		o.Lines = append(o.Lines, ol)
		lno--
	}
	return o
}

var (
	count    = 2000
	mat0Rate = 0.1
)

func main() {
	flag.IntVar(&count, "n", count, "Number of orders")
	flag.Float64Var(&mat0Rate, "r", mat0Rate, "Rate for material 0")
	flag.Parse()
	enc := json.NewEncoder(os.Stdout)
	for count > 0 {
		o := rngOrder()
		enc.Encode(o)
		count--
	}
}
