package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"

	data "codeberg.org/fractalqb/vamp-benchmarks/partialread"
)

func main() {
	var o data.Order
	dec := json.NewDecoder(os.Stdin)
	enc := json.NewEncoder(os.Stdout)
	count := 0
LOOP:
	for {
		err := dec.Decode(&o)
		switch {
		case err == io.EOF:
			break LOOP
		case err != nil:
			log.Fatal(err)
		}
		for _, ol := range o.Lines {
			if ol.MatNum == 0 {
				enc.Encode(o)
				count++
				continue LOOP
			}
		}
	}
	fmt.Fprintf(os.Stderr, "filtered %d orders\n", count)
}
