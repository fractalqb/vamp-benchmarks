#!/bin/bash

echo Build tools
cd genjson; go build; cd ..
cd json2vamp; go build; cd ..
cd mat0json; go build; cd ..
cd mat0vamp; go build; cd ..

echo Generate JSON data
genjson/genjson -n 100000 > data.json

echo Convert JSON to Vampire
json2vamp/json2vamp < data.json > data.vmp

echo Verify Vampire data
json2vamp/json2vamp -r < data.vmp > verify~
diff data.json verify~
if [ $? -ne 0 ]; then
    echo "Corrupt Vampire data"
    exit 1
fi
rm verify~
echo Vampire data OK!

echo Filter JSON for orders with MatNum 0
time mat0json/mat0json < data.json > mat0.json

echo Filter Vampire for orders with MatNum 0
time mat0vamp/mat0vamp < data.vmp > mat0.vmp

echo Verify filtered data
json2vamp/json2vamp -r < mat0.vmp > verify~
diff mat0.json verify~
if [ $? -ne 0 ]; then
    echo "Corrupt Vampire mat0"
    exit 1
fi
rm verify~
echo Filtered Vampire data OK!
