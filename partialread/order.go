package partialread

import (
	"time"

	"git.fractalqb.de/fractalqb/vamp"
)

type OrderLine struct {
	MatName string
	MatNum  uint32
	Count   int
	Price   float64
}

type Order struct {
	CustName string
	CustNum  uint32
	Date     time.Time
	Lines    []OrderLine
}

const (
	VampOffset = vamp.Size16

	LinesField  = 3
	MatNumField = 1
)

var (
	VampOrderLine = vamp.NewRecord(
		vamp.MustNamed("MatName", vamp.NewString(vamp.Size8)),
		vamp.MustNamed("MatNum", vamp.Uint32),
		vamp.MustNamed("Count", vamp.Uint32),
		vamp.MustNamed("Price", vamp.Float64),
	)
	VampOrder = vamp.NewRecord(
		vamp.MustNamed("CustName", vamp.NewString(vamp.Size8)),
		vamp.MustNamed("CustNum", vamp.Uint32),
		vamp.MustNamed("Date", vamp.Int64),
		vamp.MustNamed("Lines", vamp.NewArray(vamp.Size16, VampOrderLine)),
	)
)
