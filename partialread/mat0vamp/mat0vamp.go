package main

import (
	"fmt"
	"io"
	"log"
	"os"

	data "codeberg.org/fractalqb/vamp-benchmarks/partialread"
	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/vamp"
)

var orderList = vamp.NewList(vamp.Size32, data.VampOrder)
var count = 0

func main() {
	if len(os.Args) == 1 {
		read(os.Stdin)
	} else {
		for _, f := range os.Args[1:] {
			rd := must.Ret(os.Open(f))
			read(rd)
			rd.Close()
		}
	}
	fmt.Fprintf(os.Stderr, "filtered %d orders\n", count)
}

func read(r io.Reader) {
	vs := orderList.ReadStream(r, data.VampOffset, nil)
	if _, head, err := vs.Head(); err != nil {
		panic(err)
	} else {
		os.Stdout.Write(head)
	}
	for vs.Next() {
		obuf := vs.Buffer()
		if filter(obuf) {
			os.Stdout.Write(obuf.AllBytes())
			count++
		}
	}
	if vs.Err() != nil {
		log.Fatal(vs.Err())
	}
}

func filter(rbuf *vamp.ReadBuffer) bool {
	revert := data.VampOrder.Field(rbuf, data.LinesField)
	defer rbuf.Unslice(revert)
	linesArray := data.VampOrder.FieldType(data.LinesField).(*vamp.Array)
	numLines := linesArray.Len(rbuf)
	for i := uint(0); i < numLines; i++ {
		erev := linesArray.Index(rbuf, i)
		data.VampOrderLine.Field(rbuf, data.MatNumField)
		matNo := vamp.Int32.ReadInt32(rbuf)
		if matNo == 0 {
			return true
		}
		rbuf.Unslice(erev)
	}
	return false
}
