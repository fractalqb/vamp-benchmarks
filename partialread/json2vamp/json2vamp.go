package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"

	data "codeberg.org/fractalqb/vamp-benchmarks/partialread"
	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/vamp"
)

var orderList = vamp.NewList(vamp.Size32, data.VampOrder)

func main() {
	rev := flag.Bool("r", false, "Convert vamp to json")
	flag.Parse()
	if *rev {
		v2j(os.Stdin)
	} else {
		j2v(os.Stdin)
	}
}

func j2v(in io.Reader) {
	var o data.Order
	count := 0
	dec := json.NewDecoder(in)
	vs := must.Ret(orderList.WriteStream(os.Stdout, data.VampOffset, nil))
	defer vs.Flush()
LOOP:
	for {
		err := dec.Decode(&o)
		switch {
		case err == io.EOF:
			break LOOP
		case err != nil:
			log.Fatal(err)
		}
		err = vs.Write(o)
		if err != nil {
			log.Fatal(err)
		}
		count++
	}
	fmt.Fprintf(os.Stderr, "converted %d orders\n", count)
}

func v2j(in io.Reader) {
	vs := orderList.ReadStream(in, data.VampOffset, nil)
	enc := json.NewEncoder(os.Stdout)
	var o data.Order
	count := 0
LOOP:
	for vs.Next() {
		err := vs.Read(&o)
		switch {
		case err == io.EOF:
			break LOOP
		case err != nil:
			log.Fatal(err)
		}
		enc.Encode(o)
		count++
	}
	fmt.Fprintf(os.Stderr, "converted %d orders\n", count)
}
