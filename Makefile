# → https://blog.golang.org/cover
cover: coverage.html

coverage.html: coverage.out
	go tool cover -html=$< -o $@

coverage.out: $(GOSRC)
	go test -coverprofile=$@ ./... || true
#	go test -covermode=count -coverprofile=$@ || true

cpuprof:
	go test -cpuprofile cpu.prof -bench BenchmarkVamp_recordRW.*
	go tool pprof -http :6060 vamp-benchmarks.test cpu.prof

mprof:
	go test -memprofile mem.prof -bench BenchmarkVamp_recordRW.*
	go tool pprof -http :6060 vamp-benchmarks.test mem.prof
