package vampbenchmarks

import (
	"bytes"
	"encoding/gob"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"testing"

	"git.fractalqb.de/fractalqb/testerr"
	"git.fractalqb.de/fractalqb/vamp"
	"github.com/fxamacker/cbor/v2"
	jsoniter "github.com/json-iterator/go"
)

type ptestAnimal struct {
	Age    int      `vamp:"age,size=8"`
	Name   string   `vamp:",size=8"`
	Owners []string `vamp:",size=8:8"`
	Male   bool     `vamp:"-"`
}

var ptestType = vamp.NewRecord(
	vamp.MustNamed("age", vamp.Int8),
	vamp.MustNamed("name", vamp.NewString(vamp.Size8)),
	vamp.MustNamed("owners", vamp.NewArray(
		vamp.Size8,
		vamp.NewString(vamp.Size8),
	)),
	vamp.MustNamed("male", vamp.Bool),
)

func Example_size() {
	buf := vamp.NewTypeWriteBuffer(vamp.Size16, 0, nil)
	vamp.WriteType(buf, ptestType)
	fmt.Println("Type:\t", len(buf.Bytes()), "byte", ptestType.String())
	in := ptestAnimal{
		Age:    4,
		Name:   "Candy",
		Owners: []string{"Mary", "Joe"},
		Male:   true,
	}
	buf.Reset(ptestType, vamp.Size16, 0)
	err := ptestType.Write(buf, in)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Vamp:\t", len(buf.Bytes()), "byte",
		hex.EncodeToString(buf.Bytes()))
	res, err := json.Marshal(in)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("JSON:\t", len(res), "byte", string(res))
	res, err = jsoniter.Marshal(in)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Jsoniter:", len(res), "byte", string(res))
	var gobb bytes.Buffer
	err = gob.NewEncoder(&gobb).Encode(in)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("GOB :\t", gobb.Len(), "byte")
	res, err = cbor.Marshal(in)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("CBOR:\t", len(res), "byte")
	// Output:
	// Type:	 54 byte {age:int8 name:"Size8" owners:[Size8 "Size8"] male:bool}
	// Vamp:	 26 byte 040504000206000143616e64790403000304004d6172794a6f65
	// JSON:	 60 byte {"Age":4,"Name":"Candy","Owners":["Mary","Joe"],"Male":true}
	// Jsoniter: 60 byte {"Age":4,"Name":"Candy","Owners":["Mary","Joe"],"Male":true}
	// GOB :	 112 byte
	// CBOR:	 40 byte
}

func BenchmarkVamp_recordRWreflect(b *testing.B) {
	in := ptestAnimal{
		Age:    4,
		Name:   "Candy",
		Owners: []string{"Mary", "Joe"},
		Male:   true,
	}
	_, inFields, err := vamp.ReflectType(in)
	testerr.Shall(err).BeNil(b)
	vamp.UseFieldsForStruct(in, inFields)
	var buf vamp.Buffer
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf.Reset(ptestType, vamp.Size16, 0)
		ptestType.Write(&buf, in)
		var out ptestAnimal
		ptestType.Read(&buf.ReadBuffer, &out)
	}
}

func BenchmarkVamp_recordRW(b *testing.B) {
	in := ptestAnimal{
		Age:    4,
		Name:   "Candy",
		Owners: []string{"Mary", "Joe"},
		Male:   true,
	}
	_, inFields, err := vamp.ReflectType(in)
	testerr.Shall(err).BeNil(b)

	var buf vamp.Buffer
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf.Reset(ptestType, vamp.Size16, 0)
		ptestType.WriteFields(&buf, inFields.Get(in, nil)...)
		var out ptestAnimal
		ptestType.ReadFields(&buf.ReadBuffer, inFields.Set(&out, nil)...)
	}
}

func BenchmarkVamp_recordRWreuse(b *testing.B) {
	in := ptestAnimal{
		Age:    4,
		Name:   "Candy",
		Owners: []string{"Mary", "Joe"},
		Male:   true,
	}
	_, inFields, err := vamp.ReflectType(in)
	testerr.Shall(err).BeNil(b)

	var (
		buf vamp.Buffer
		fs  []any
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf.Reset(ptestType, vamp.Size16, 0)
		fs = inFields.Get(in, fs)
		ptestType.WriteFields(&buf, fs...)
		var out ptestAnimal
		fs = inFields.Set(&out, fs)
		ptestType.ReadFields(&buf.ReadBuffer, fs...)
	}
}

func BenchmarkVamp_recordRWfields(b *testing.B) {
	in := ptestAnimal{
		Age:    4,
		Name:   "Candy",
		Owners: []string{"Mary", "Joe"},
		Male:   true,
	}
	var buf vamp.Buffer
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf.Reset(ptestType, vamp.Size16, 0)
		ptestType.WriteFields(&buf,
			in.Age,
			in.Name,
			in.Owners,
			in.Male,
		)
		var out ptestAnimal
		ptestType.ReadFields(&buf.ReadBuffer,
			&out.Age,
			&out.Name,
			&out.Owners,
			&out.Male,
		)
	}
}

func BenchmarkCBOR(b *testing.B) {
	in := ptestAnimal{Age: 4, Name: "Candy", Owners: []string{"Mary", "Joe"}, Male: true}
	var buf []byte
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf, _ = cbor.Marshal(in)
		var out ptestAnimal
		cbor.Unmarshal(buf, &out)
	}
}

func BenchmarkJSON(b *testing.B) {
	in := ptestAnimal{Age: 4, Name: "Candy", Owners: []string{"Mary", "Joe"}, Male: true}
	var buf []byte
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf, _ = json.Marshal(in)
		var out ptestAnimal
		json.Unmarshal(buf, &out)
	}
}

func BenchmarkJsoniter(b *testing.B) {
	in := ptestAnimal{Age: 4, Name: "Candy", Owners: []string{"Mary", "Joe"}, Male: true}
	var buf []byte
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf, _ = jsoniter.Marshal(in)
		var out ptestAnimal
		jsoniter.Unmarshal(buf, &out)
	}
}

func BenchmarkGOB(b *testing.B) {
	in := ptestAnimal{Age: 4, Name: "Candy", Owners: []string{"Mary", "Joe"}, Male: true}
	var buf bytes.Buffer
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf.Reset()
		gob.NewEncoder(&buf).Encode(in)
		var out ptestAnimal
		gob.NewDecoder(&buf).Decode(&out)
	}
}
