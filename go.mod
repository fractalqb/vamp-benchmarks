module codeberg.org/fractalqb/vamp-benchmarks

go 1.22.0

toolchain go1.23.1

require (
	git.fractalqb.de/fractalqb/eloc v0.3.0
	git.fractalqb.de/fractalqb/testerr v0.1.1
	git.fractalqb.de/fractalqb/vamp v0.8.0
	github.com/fxamacker/cbor/v2 v2.7.0
)

require (
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/exp v0.0.0-20241009180824-f66d83c29e7c // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/json-iterator/go v1.1.12
	github.com/x448/float16 v0.8.4 // indirect
)
